package com.emiram.sneeze;

import androidx.appcompat.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private Button sneezeButton;
    private Button resetButton;
    private Button saveButton;
    private Button resetTableButton;
    private Button sneeze2Button;
    private Button sneeze3Button;
    private TextView counterView;
    private TextView tableView1;
    private TextView tableView2;
    private TextView tableView3;
    private int counter = 0;
    private int savedCounter1;
    private int savedCounter2;
    private int savedCounter3;
    private Toast toast;
    private static final String PREFS_NAME = "sharedPrefs";
    private Boolean tableViewSet1 = false;
    private Boolean tableViewSet2 = false;
    private Boolean tableViewSet3 = false;
    private Vibrator vibration;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide(); // hide the title bar
        vibration = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        initElements();
        int duration = Toast.LENGTH_SHORT;
        toast = Toast.makeText(this, "Bless you!", duration);
        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, 0);
        int counterShared = sharedPreferences.getInt("CounterKey", 0);
        // saveData();
        counterView.setText(String.valueOf(counterShared));
        counter = counterShared;
        sneezeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                counterView.setText(String.valueOf(counter));
                toast.show();
                vibrateHere();
            }
        });

        sneeze2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 2;
                counterView.setText(String.valueOf(counter));
                toast.show();
                vibrateHere();
            }
        });

        sneeze3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 3;
                counterView.setText(String.valueOf(counter));
                toast.show();
                vibrateHere();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = 0;
                counterView.setText(String.valueOf(counter));
            }
        });

        tableViewSet1 = sharedPreferences.getBoolean("Tableview1", false);
        int savedCounter1Shared = sharedPreferences.getInt("SavedCounterKey1", 0);
        savedCounter1 = savedCounter1Shared;
        if (tableViewSet1) {
            tableView1.setText("Entry 1: " + savedCounter1);
        }
        tableViewSet2 = sharedPreferences.getBoolean("Tableview2", false);
        int savedCounter2Shared = sharedPreferences.getInt("SavedCounterKey2", 0);
        savedCounter2 = savedCounter2Shared;
        if (tableViewSet2) {
            tableView2.setText("Entry 2: " + savedCounter2);
        }
        tableViewSet3 = sharedPreferences.getBoolean("Tableview3", false);
        int savedCounter3Shared = sharedPreferences.getInt("SavedCounterKey3", 0);
        savedCounter3 = savedCounter3Shared;
        if (tableViewSet3) {
            tableView3.setText("Entry 3: " + savedCounter3);
        }

        if (tableViewSet1 || tableViewSet2 || tableViewSet3) {
            resetTableButton.setVisibility(View.VISIBLE);
        }

        if (!tableViewSet1 || !tableViewSet2 || !tableViewSet3) {
            saveButton.setText(getString(R.string.button_save));
            saveButton.setBackground(getDrawable(R.drawable.shape));
            saveButton.setClickable(true);
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tableViewSet1) {
                    savedCounter1 = counter;
                    tableView1.setText("Entry 1: " + String.valueOf(savedCounter1));
                    counter = 0;
                    counterView.setText(String.valueOf(counter));
                    tableViewSet1 = true;
                } else {
                    if (!tableViewSet2) {
                        savedCounter2 = counter;
                        tableView2.setText("Entry 2: " + String.valueOf(savedCounter2));
                        counter = 0;
                        counterView.setText(String.valueOf(counter));
                        tableViewSet2 = true;
                    } else {
                        if (!tableViewSet3) {
                            savedCounter3 = counter;
                            tableView3.setText("Entry 3: " + String.valueOf(savedCounter3));
                            counter = 0;
                            counterView.setText(String.valueOf(counter));
                            tableViewSet3 = true;
                        }
                    }
                }
                resetTableButton.setVisibility(View.VISIBLE);
                if (tableViewSet1 && tableViewSet2 && tableViewSet3) {
                    // saveButton.setVisibility(View.GONE);
                    saveButton.setText(getString(R.string.button_resetTableFirst));
                    saveButton.setBackground(getDrawable(R.drawable.shape_gray));
                    saveButton.setClickable(false);
                }
            }
        });

        resetTableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tableView1.setText("");
                tableViewSet1 = false;
                tableView2.setText("");
                tableViewSet2 = false;
                tableView3.setText("");
                tableViewSet3 = false;
                resetTableButton.setVisibility(View.GONE);
                saveButton.setText(getString(R.string.button_save));
                saveButton.setBackground(getDrawable(R.drawable.shape));
                saveButton.setClickable(true);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putInt("CounterKey", counter);
        editor.putInt("SavedCounterKey1", savedCounter1);
        editor.putInt("SavedCounterKey2", savedCounter2);
        editor.putInt("SavedCounterKey3", savedCounter3);
        System.out.println(savedCounter1 +" "+ savedCounter2 + " " + savedCounter3);
        System.out.println(counter);
        editor.putBoolean("Tableview1", tableViewSet1);
        editor.putBoolean("Tableview2", tableViewSet2);
        editor.putBoolean("Tableview3", tableViewSet3);
        editor.apply();
    }

    public void vibrateHere() {
        vibration.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
    }

    public void initElements() {
        sneezeButton = findViewById(R.id.sneezeButton);
        sneeze2Button = findViewById(R.id.sneeze2Button);
        sneeze3Button = findViewById(R.id.sneeze3Button);
        counterView = findViewById(R.id.counterView);
        tableView1 = findViewById(R.id.tableView1);
        tableView2 = findViewById(R.id.tableView2);
        tableView3 = findViewById(R.id.tableView3);
        resetButton = findViewById(R.id.resetButton);
        saveButton = findViewById(R.id.saveButton);
        resetTableButton = findViewById(R.id.resetTableButton);
        if (!tableViewSet1 && !tableViewSet2 && !tableViewSet3) {
            resetTableButton.setVisibility(View.GONE);
        } else {
            resetTableButton.setVisibility(View.VISIBLE);
        }
    }
}
